import { useState } from 'react'
import Card, {params as paramsCard} from './components/Card'
import Grid from './components/Grid'

const App = (): JSX.Element => {

  const [data, setData] = useState<paramsCard[]>([
    {value:"1",image:'/vite.svg' ,title:'Tienda',description:'descripcion_test'},
    {value:"2",image:'/vite.svg' ,title:'Tienda2',description:'descripcion_test2'},
    {value:"3",image:'/vite.svg' ,title:'Tienda3',description:'descripcion_test3'},
    {value:"4",image:'/vite.svg' ,title:'Tienda4',description:'descripcion_test4'},
    {value:"5",image:'/vite.svg' ,title:'Tienda5',description:'descripcion_test5'},
    {value:"6",image:'/vite.svg' ,title:'Tienda6',description:'descripcion_test6'},
    {value:"7",image:'/vite.svg' ,title:'Tienda7',description:'descripcion_test7'}

  ])

  const handleSelect:paramsCard['onClick'] = (e) => {
    console.log(e);
  }
  return (
    <Grid>
      {
        data.map((v,i)=> <Card {...v} onClick={handleSelect}/>)
      }
    </Grid>
  )
}

export default App
