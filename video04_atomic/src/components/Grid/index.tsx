import styled from "styled-components";

const Grid = styled.div`
    position: relative;
    flex-direction:row;
    flex-wrap:wrap;
    justify-content; space-evenly;
    align-self:center;
    align-content:flex-start;
    display:flex;
    width:100%;
    height:100%;
    overflow:scroll;
    gap: 15px;
`

export default Grid;